create table ticket (
  id SERIAL,
  qr_code varchar(50),
  valid_thru timestamp,
  buy_time timestamp,
  ticket_type varchar(50),
  reduced boolean,
  price numeric,
  multiplier numeric,
  user_id numeric
);

CREATE INDEX ticket_buy_time_idx ON ticket(buy_time);
