package pl.thepenguins.antismog.tickets;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long> {

    List<Ticket> findByBuyTimeBetween(LocalDateTime start, LocalDateTime end);

    List<Ticket> findAllByUserId(Long userId);
}
