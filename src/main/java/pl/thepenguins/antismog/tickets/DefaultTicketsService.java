package pl.thepenguins.antismog.tickets;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.thepenguins.antismog.api.command.BuyTicketCommand;
import pl.thepenguins.antismog.api.dto.TicketDto;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DefaultTicketsService implements TicketsService {

    @Autowired
    TicketRepository ticketRepository;

    @Override
    public TicketDto buyTicket(BuyTicketCommand buyTicketCommand) {
        Ticket ticket = new Ticket();
        TicketType ticketType = buyTicketCommand.getTicketType();
        LocalDateTime now = LocalDateTime.now();
        ticket.setUserId(buyTicketCommand.getUserId());
        ticket.setBuyTime(now);
        ticket.setValidThru(now.plusMinutes(ticketType.minutes));
        ticket.setMultiplier(BigDecimal.valueOf(buyTicketCommand.getDrawCount()));
        ticket.setPrice(buyTicketCommand.isReduced()?ticketType.price.multiply(new BigDecimal("0.5")):ticketType.price);
        ticket.setTicketType(ticketType);

        return TicketDto.from(ticketRepository.save(ticket));
    }

    public List<TicketDto> getUserTickets(Long userId) {
        return ticketRepository.findAllByUserId(userId).stream().map(TicketDto::from).collect(Collectors.toList());
    }

    @Override
    public List<Ticket> getTodaysTickets() {
        LocalDateTime now = LocalDateTime.now();
        return ticketRepository.findByBuyTimeBetween(now.toLocalDate().atStartOfDay(), now.toLocalDate().plusDays(1).atStartOfDay());
    }


}
