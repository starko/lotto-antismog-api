package pl.thepenguins.antismog.tickets;

import java.math.BigDecimal;

public enum TicketType {
    SINGLE_20(20, new BigDecimal("3.40")),
    SINGLE_75(75, new BigDecimal("4.40")),
    SINGLE_90(90, new BigDecimal("7.00"));

    public final int minutes;
    public final BigDecimal price;

    TicketType(int minutes, BigDecimal price) {
        this.minutes = minutes;
        this.price = price;
    }

}
