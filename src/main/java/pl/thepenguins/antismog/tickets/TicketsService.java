package pl.thepenguins.antismog.tickets;

import pl.thepenguins.antismog.api.command.BuyTicketCommand;
import pl.thepenguins.antismog.api.dto.TicketDto;

import java.util.List;

public interface TicketsService {
    List<Ticket> getTodaysTickets();

    TicketDto buyTicket(BuyTicketCommand buyTicketCommand);

    List<TicketDto> getUserTickets(Long userId);
}
