package pl.thepenguins.antismog.tickets;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String qrCode = null;

    private LocalDateTime validThru = null;

    private LocalDateTime buyTime = null;

    private BigDecimal multiplier = null;

    private BigDecimal price = null;

    private TicketType ticketType = null;

    private Long userId;

}
