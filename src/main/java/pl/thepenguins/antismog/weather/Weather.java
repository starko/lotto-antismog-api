package pl.thepenguins.antismog.weather;

import lombok.Data;

@Data
public class Weather {
    private final double windStrength;
    private final double rain;
    private final double pressure;
    private final double clouds;
    private final double temp;
    private final String description;
    private final String icon;

    public static Weather empty = new Weather(0, 0, 0, 0, 0, "", "");
    public static Weather any = new Weather(0, 0, 0, 0, 2, "Mgła", "701");
}
