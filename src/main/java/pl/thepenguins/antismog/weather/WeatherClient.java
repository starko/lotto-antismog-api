package pl.thepenguins.antismog.weather;

import java.util.Optional;

public interface WeatherClient {
    Optional<Weather> getWeather();
}
