package pl.thepenguins.antismog.weather;

import lombok.AllArgsConstructor;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Optional;

@Component
@AllArgsConstructor
public class OpenWeatherMapClient {
    private final String apiKey = "f0a9ff071349eea75c869c0106631782";

    public Weather getWeather() throws IOException, JSONException {
        URL url = new URL("https://api.openweathermap.org/data/2.5/weather?units=metric&lang=pl&q=Warsaw,pl&APPID=" + apiKey);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestProperty("Accept", "application/json");
        con.setRequestMethod("GET");
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder content = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();
        con.disconnect();
        JSONObject jsonObject = new JSONObject(content.toString());

        JSONObject main = jsonObject.getJSONObject("main");
        JSONObject weather = jsonObject.getJSONArray("weather").getJSONObject(0);
        String description = weather.getString("description");
        String id = String.valueOf(weather.getInt("id"));
        Double temp = this.getDouble(main, "temp").orElse(0D);
        Double pressure = this.getDouble(main, "pressure").orElse(0D);
        Double wind = this.getJSONObject(jsonObject, "wind").flatMap(jo -> this.getDouble(jo, "speed")).orElse(0D);
        Double clouds = this.getJSONObject(jsonObject, "clouds").flatMap(jo -> this.getDouble(jo, "all")).orElse(0D);
        Double rain = this.getJSONObject(jsonObject, "rain").flatMap(jo -> this.getDouble(jo, "h1")).orElse(0D);

        return new Weather(wind, rain, pressure, clouds, temp, description, id);
    }

    Optional<JSONObject> getJSONObject(JSONObject jsonObject, String name) {
        return jsonObject.has(name) ? Optional.ofNullable(jsonObject.getJSONObject(name)) : Optional.empty();
    }

    Optional<Double> getDouble(JSONObject jsonObject, String name) {
        return jsonObject.has(name) ? Optional.of(jsonObject.getDouble(name)) : Optional.of(0D);
    }

    public static void main(String... args) throws IOException, JSONException {
        System.out.println(new OpenWeatherMapClient().getWeather());
    }
}
