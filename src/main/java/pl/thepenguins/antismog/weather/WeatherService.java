package pl.thepenguins.antismog.weather;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
@AllArgsConstructor
public class WeatherService {

    private final WeatherClient weatherClient;
    private Optional<Weather> weather = Optional.empty();

    @Scheduled(fixedRate = 60000)
    public void refreshWeather() {
        log.info("Refresh weather info");
        Optional<Weather> newWeather = weatherClient.getWeather();
        newWeather.ifPresent(refreshedWeather -> weather = Optional.of(refreshedWeather));
    }

    public Weather getWeather() {
        return this.weather.orElse(Weather.any);
    }

}
