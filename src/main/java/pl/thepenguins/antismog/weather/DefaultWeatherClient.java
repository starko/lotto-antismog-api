package pl.thepenguins.antismog.weather;

import lombok.AllArgsConstructor;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Optional;

@Component
@AllArgsConstructor
public class DefaultWeatherClient implements WeatherClient {

    @Autowired
    private final OpenWeatherMapClient openWeatherMapClient;

    @Override
    public Optional<Weather> getWeather() {
        try {
            return Optional.of(openWeatherMapClient.getWeather());
        } catch (IOException | JSONException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }
}
