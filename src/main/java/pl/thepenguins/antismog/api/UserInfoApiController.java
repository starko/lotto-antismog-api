package pl.thepenguins.antismog.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import pl.thepenguins.antismog.api.dto.TrafficInfoDto;
import pl.thepenguins.antismog.api.dto.UserInfoDto;
import pl.thepenguins.antismog.lottery.CurrentLocation;
import pl.thepenguins.antismog.lottery.LotteryService;
import pl.thepenguins.antismog.smog.SmogClient;
import pl.thepenguins.antismog.weather.WeatherClient;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2018-11-24T19:47:27.622Z[GMT]")

@RestController
public class UserInfoApiController implements UserInfoApi {

    private static final Logger log = LoggerFactory.getLogger(UserInfoApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    private final LotteryService lotteryService;

    private final WeatherClient weatherClient;

    private final SmogClient smogClient;

    @org.springframework.beans.factory.annotation.Autowired
    public UserInfoApiController(ObjectMapper objectMapper,
                                 HttpServletRequest request,
                                 LotteryService lotteryService,
                                 WeatherClient weatherClient,
                                 SmogClient smogClient) {
        this.objectMapper = objectMapper;
        this.request = request;
        this.lotteryService = lotteryService;
        this.weatherClient = weatherClient;
        this.smogClient = smogClient;
    }

    public ResponseEntity<UserInfoDto> getUserInfo() {
        String accept = request.getHeader("Accept");
        BigDecimal chanceMultiplier = lotteryService.getChanceMultiplier(new CurrentLocation());
        TrafficInfoDto trafficInfo = new TrafficInfoDto();
        UserInfoDto userInfo =
                new UserInfoDto(
                        chanceMultiplier,
                        trafficInfo
                );
        return new ResponseEntity<UserInfoDto>(userInfo, HttpStatus.OK);
    }

}
