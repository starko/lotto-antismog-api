package pl.thepenguins.antismog.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import org.springframework.validation.annotation.Validated;
import pl.thepenguins.antismog.tickets.Ticket;
import pl.thepenguins.antismog.tickets.TicketType;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Ticket
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2018-11-24T19:47:27.622Z[GMT]")
@Builder
@Data
public class TicketDto {
    @JsonProperty("qrCode")
    private String qrCode = null;

    @JsonProperty("validThru")
    private LocalDateTime validThru = null;

    @JsonProperty("buyTime")
    private LocalDateTime buyTime = null;

    @JsonProperty("price")
    private BigDecimal price = null;

    @JsonProperty("multiplier")
    private BigDecimal multiplier = null;

    @JsonProperty("type")
    private TicketType ticketType = null;

    public static TicketDto from(Ticket ticket) {
        return TicketDto.builder()
                .buyTime(ticket.getBuyTime())
                .multiplier(ticket.getMultiplier())
                .qrCode(ticket.getQrCode())
                .price(ticket.getPrice())
                .ticketType(ticket.getTicketType())
                .multiplier(ticket.getMultiplier())
                .validThru(ticket.getValidThru()).build();
    }
}

