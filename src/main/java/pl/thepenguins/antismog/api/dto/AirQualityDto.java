package pl.thepenguins.antismog.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.thepenguins.antismog.smog.SmogLevel;

import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class AirQualityDto {
    private SmogLevel smogLevel;
    private BigDecimal pm10;
    private BigDecimal pm25;
}

