package pl.thepenguins.antismog.api.dto;

import lombok.Data;

@Data
public class TrafficInfoDto {
    private String trafficLocation;
    private String trafficDescription;
}

