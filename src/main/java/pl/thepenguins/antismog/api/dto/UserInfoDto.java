package pl.thepenguins.antismog.api.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * UserInfo
 */

@Data
public class UserInfoDto {

    private final BigDecimal chanceMultiplier;

    private final TrafficInfoDto trafficInfo;

}

