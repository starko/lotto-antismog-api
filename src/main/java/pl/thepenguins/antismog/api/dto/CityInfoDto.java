package pl.thepenguins.antismog.api.dto;

import lombok.Builder;
import lombok.Data;
import org.threeten.bp.OffsetDateTime;

@Data
@Builder
public class CityInfoDto {
    private final String name;
    private final int currentLotteryValue;

    private final AirQualityDto airQuality;
    private final WeatherInfoDto weather;
    private final OffsetDateTime currentTime;

    private final int smogDotation;
    private final int weatherDotation;
}

