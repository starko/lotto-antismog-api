package pl.thepenguins.antismog.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class WeatherInfoDto {
    private double temp;
    private String kind;
    private String icon;
}

