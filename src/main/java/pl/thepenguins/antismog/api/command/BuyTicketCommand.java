package pl.thepenguins.antismog.api.command;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.thepenguins.antismog.tickets.TicketType;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class BuyTicketCommand {
    Long userId;
    TicketType ticketType;
    boolean reduced;
    int drawCount;
}
