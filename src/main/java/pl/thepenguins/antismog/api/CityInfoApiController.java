package pl.thepenguins.antismog.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.threeten.bp.OffsetDateTime;
import pl.thepenguins.antismog.api.dto.AirQualityDto;
import pl.thepenguins.antismog.api.dto.CityInfoDto;
import pl.thepenguins.antismog.api.dto.WeatherInfoDto;
import pl.thepenguins.antismog.lottery.CurrentLotteryValueService;
import pl.thepenguins.antismog.lottery.LotteryService;
import pl.thepenguins.antismog.smog.SmogIndexMeasurement;
import pl.thepenguins.antismog.smog.SmogMeasurment;
import pl.thepenguins.antismog.smog.SmogService;
import pl.thepenguins.antismog.weather.Weather;
import pl.thepenguins.antismog.weather.WeatherClient;
import pl.thepenguins.antismog.weather.WeatherService;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Optional;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2018-11-24T19:47:27.622Z[GMT]")

@RestController
public class CityInfoApiController implements CityInfoApi {

    private static final Logger log = LoggerFactory.getLogger(CityInfoApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    private final WeatherService weatherService;

    private final SmogService smogService;

    private final LotteryService lotteryService;

    private final CurrentLotteryValueService currentLotteryValueService;

    @org.springframework.beans.factory.annotation.Autowired
    public CityInfoApiController(ObjectMapper objectMapper,
                                 HttpServletRequest request,
                                 WeatherService weatherService,
                                 SmogService smogService,
                                 LotteryService lotteryService,
                                 CurrentLotteryValueService currentLotteryValueService) {
        this.objectMapper = objectMapper;
        this.request = request;
        this.weatherService = weatherService;
        this.smogService = smogService;
        this.lotteryService = lotteryService;
        this.currentLotteryValueService = currentLotteryValueService;
    }

    public ResponseEntity<CityInfoDto> getCityInfo() {
        Weather weather = weatherService.getWeather();

        int currentLotteryValue = lotteryService.getCurrentLotteryValue(
                        smogService.getSmogMeasurment(),
                        weather);

            SmogMeasurment smogLevels = smogService.getSmogMeasurment();
            CityInfoDto cityInfo = CityInfoDto.builder()
                        .name("Warszawa")
                        .currentLotteryValue(currentLotteryValue)
                        .airQuality(getAirQuality(smogLevels))
                        .weather(getWeatherInfo(weather))
                        .currentTime(OffsetDateTime.now())
                        .smogDotation(this.currentLotteryValueService.smogDotation(smogLevels))
                        .weatherDotation(this.currentLotteryValueService.weatherDotation(weather))
                        .build();

                return new ResponseEntity<>(
                        cityInfo,
                        HttpStatus.OK);
    }

    private WeatherInfoDto getWeatherInfo(Weather weather) {
        return  WeatherInfoDto.builder()
                        .temp(weather.getTemp())
                        .icon(weather.getIcon())
                        .kind(weather.getDescription()).build();
    }

    private AirQualityDto getAirQuality(SmogMeasurment smogMeasurment) {
        return AirQualityDto.builder()
                .smogLevel(smogMeasurment.getSmogLevel())
                .pm25(smogMeasurment.getIndexMeasurements().stream().filter(m -> m.getIndexName().equals("PM25")).findFirst().map(SmogIndexMeasurement::getValue)
                        .orElse(BigDecimal.ZERO))
                .pm10(smogMeasurment.getIndexMeasurements().stream().filter(m -> m.getIndexName().equals("PM10")).findFirst().map(SmogIndexMeasurement::getValue)
                                .orElse(BigDecimal.ZERO))
                .build();
    }

}
