package pl.thepenguins.antismog.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.thepenguins.antismog.api.command.BuyTicketCommand;
import pl.thepenguins.antismog.api.dto.TicketDto;
import pl.thepenguins.antismog.tickets.Ticket;
import pl.thepenguins.antismog.tickets.TicketsService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2018-11-24T19:47:27.622Z[GMT]")

@RestController
public class TicketsApiController implements TicketsApi {

    private static final Logger log = LoggerFactory.getLogger(TicketsApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    private final TicketsService ticketsService;

    @org.springframework.beans.factory.annotation.Autowired
    public TicketsApiController(ObjectMapper objectMapper, HttpServletRequest request, TicketsService ticketsService) {
        this.objectMapper = objectMapper;
        this.request = request;
        this.ticketsService = ticketsService;
    }

    public ResponseEntity<TicketDto> buyTicket(@RequestBody BuyTicketCommand buyTicketCommand) {
        String accept = request.getHeader("Accept");
        TicketDto ticket = this.ticketsService.buyTicket(buyTicketCommand);

        return new ResponseEntity<TicketDto>(ticket, HttpStatus.CREATED);
    }

    public ResponseEntity<List<TicketDto>> getUserTickets(Long userId) {
        String accept = request.getHeader("Accept");

        List<TicketDto> userTickets = this.ticketsService.getUserTickets(userId);
        return new ResponseEntity<>(userTickets, HttpStatus.OK);
    }


}
