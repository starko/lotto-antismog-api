package pl.thepenguins.antismog.lottery;

import pl.thepenguins.antismog.smog.SmogMeasurment;
import pl.thepenguins.antismog.weather.Weather;

import java.math.BigDecimal;

public interface LotteryService {

    int getCurrentLotteryValue(SmogMeasurment smog, Weather weather);

    BigDecimal getChanceMultiplier(CurrentLocation currentLocation);
}
