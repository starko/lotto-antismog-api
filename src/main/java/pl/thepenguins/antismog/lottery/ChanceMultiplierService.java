package pl.thepenguins.antismog.lottery;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

@Service
public class ChanceMultiplierService {

    private long lastRefresh = System.currentTimeMillis();
    private int position = 0;
    private List<Integer> multipliers = Arrays.asList(1, 2, 3, 4, 5, 4, 3, 2);

    public BigDecimal getChanceMultiplier(CurrentLocation currentLocation) {
        if (System.currentTimeMillis() - lastRefresh > 15_000) {
            position++;
            if (position == 7) {
                position = 0;
            }
        }

        return BigDecimal.valueOf(multipliers.get(position));
    }
}
