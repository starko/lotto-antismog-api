package pl.thepenguins.antismog.lottery;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.thepenguins.antismog.smog.SmogMeasurment;
import pl.thepenguins.antismog.weather.Weather;

import java.math.BigDecimal;

@Service
@AllArgsConstructor
public class DefaultLotteryService implements LotteryService {

    @Autowired
    private final ChanceMultiplierService chanceMultiplierService;

    @Autowired
    private final CurrentLotteryValueService currentLotteryValueService;

    @Override
    public int getCurrentLotteryValue(SmogMeasurment smog, Weather weather) {
        return currentLotteryValueService.getCurrentLotteryValue(smog, weather);
    }

    @Override
    public BigDecimal getChanceMultiplier(CurrentLocation currentLocation) {
        return chanceMultiplierService.getChanceMultiplier(currentLocation);
    }
}
