package pl.thepenguins.antismog.lottery;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.thepenguins.antismog.smog.SmogMeasurment;
import pl.thepenguins.antismog.tickets.Ticket;
import pl.thepenguins.antismog.tickets.TicketsService;
import pl.thepenguins.antismog.weather.Weather;

import java.math.BigDecimal;
import java.util.List;

@Service
@AllArgsConstructor
public class CurrentLotteryValueService {

    @Autowired
    private final TicketsService ticketsService;

    private final double cityBudget = 1_000_000;

    private final double maxDailyDotation = cityBudget / 365;

    private final double initialDailyBudget = 15_000;

    public int getCurrentLotteryValue(SmogMeasurment smog, Weather weather) {
        double dailyWinningPot =
                    initialDailyBudget +
                    smogDotation(smog) +
                    weatherDotation(weather) +
                    getTicketPot(smog, weather);

        return BigDecimal.valueOf(dailyWinningPot).intValue();
    }

    private double getTicketPot(SmogMeasurment smog, Weather weather) {
        return (this.getSmogMultiplier(smog) + getWeatherMultiplier(weather)) / 6 * getTicketsValue();
    }

    private double getTicketsValue() {
        List<Ticket> todaysTickets = ticketsService.getTodaysTickets();

        return todaysTickets.stream()
                .map(Ticket::getPrice)
                .mapToDouble(BigDecimal::doubleValue)
                .sum();
    }

    public int weatherDotation(Weather weather) {
        double weatherMultiplier = this.getWeatherMultiplier(weather);
        return new Double((0.3 * weatherMultiplier) * maxDailyDotation).intValue();
    }

    public int smogDotation(SmogMeasurment smog) {
        double smogMultiplier = this.getSmogMultiplier(smog);
        return new Double((0.7 * smogMultiplier) * maxDailyDotation).intValue();
    }

    private double getWeatherMultiplier(Weather weather) {
        double sum = 0;
        if( weather.getClouds() > 1 ) {
            sum = sum + 1;
        }
        if(  weather.getRain() > 1 ) {
            sum = sum + 1;
        }
        if(  weather.getWindStrength() > 1 ) {
            sum = sum + 1;
        }
        if( shittyWeather(weather)) {
            sum =  sum + 1;
        }

        return sum == 3 ? 4 : sum;
    }

    private boolean shittyWeather(Weather weather) {
        return weather.getClouds() > 1 && weather.getRain() > 1 && weather.getWindStrength() > 1 ;
    }

    private double getSmogMultiplier(SmogMeasurment smog) {
        if(smog.getSmogLevel() == null) {
            return 1.00;
        } else {
            return smog.getSmogLevel().ordinal();
        }
    }
}
