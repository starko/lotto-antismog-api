package pl.thepenguins.antismog.smog;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Component
@NoArgsConstructor
@AllArgsConstructor
public class DefaultAirlyClient implements AirlyClient {

    @Value("${airly.apikey}")
    private String apiKey;

    @Override
    public SmogMeasurment getMeasurements() throws IOException, JSONException {
        URL url = new URL("https://airapi.airly.eu/v2/measurements/nearest?lat=52.2297&lng=21.0122&maxDistanceKM=5&maxResults=1");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestProperty("Accept", "application/json");
        con.setRequestProperty("apikey", apiKey);
        con.setRequestMethod("GET");
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder content = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();
        con.disconnect();
        JSONObject jsonObject = new JSONObject(content.toString());

        String level = jsonObject.getJSONObject("current").getJSONArray("indexes").getJSONObject(0).getString("level");

        List<SmogIndexMeasurement> indexMeasurments = jsonObject
                .getJSONObject("current")
                .getJSONArray("values")
                .toList().stream().map(value -> {
                            Map mapValue = (Map) value;
                            return SmogIndexMeasurement.builder()
                                    .indexName((String) mapValue.get("name"))
                                    .value(new BigDecimal((Double) mapValue.get("value"))).build();
                        }

                ).collect(Collectors.toList());


        return SmogMeasurment.builder()
                .smogLevel(SmogLevel.valueOf(level))
                .indexMeasurements(indexMeasurments).build();


    }

    public static void main(String... args) throws IOException, JSONException {
        System.out.println(new DefaultAirlyClient("XXX").getMeasurements());
    }
}
