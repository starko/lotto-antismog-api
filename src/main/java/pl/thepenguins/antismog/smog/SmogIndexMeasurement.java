package pl.thepenguins.antismog.smog;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Builder
@AllArgsConstructor
@Data
public class SmogIndexMeasurement {
    String indexName;
    BigDecimal value;
}
