package pl.thepenguins.antismog.smog;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Slf4j
@Service
public class SmogService {

    private final AirlyClient airlyClient;
    private SmogMeasurment smog;

    public SmogService(AirlyClient airlyClient) {
        this.airlyClient = airlyClient;
    }

    public SmogMeasurment getSmogMeasurment() {
        return this.smog;
    }

    @Scheduled(fixedRate = 60000)
    public void refreshSmogLevels() {
        log.info("Refreshing smog levels...");
        try {
            this.smog = airlyClient.getMeasurements();
            log.info("Current smog levels: {}", this.smog);
        } catch (IOException e) {
            log.error(e.getMessage() ,e);
        }
    }
}
