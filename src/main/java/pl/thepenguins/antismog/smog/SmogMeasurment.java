package pl.thepenguins.antismog.smog;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@AllArgsConstructor
@Builder
@Data
public class SmogMeasurment {
    private final SmogLevel smogLevel;
    private final List<SmogIndexMeasurement> indexMeasurements;

}
