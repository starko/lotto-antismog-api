package pl.thepenguins.antismog.smog;

public enum SmogLevel {
    LOW, MEDIUM, HIGH
}
