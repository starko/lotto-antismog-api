package pl.thepenguins.antismog.smog;

import java.util.Optional;

public interface SmogClient {
    Optional<SmogMeasurment> getSmogLevels();
}
