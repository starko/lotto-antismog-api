package pl.thepenguins.antismog.smog;

import lombok.AllArgsConstructor;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Optional;

@Component
@AllArgsConstructor
public class DefaultSmogClient implements SmogClient {

    @Autowired
    private final DefaultAirlyClient airlyClient;

    @Override
    public Optional<SmogMeasurment> getSmogLevels() {
        try {
            return Optional.of(airlyClient.getMeasurements());
        } catch (IOException | JSONException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }
}
