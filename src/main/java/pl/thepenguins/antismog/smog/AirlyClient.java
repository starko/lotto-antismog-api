package pl.thepenguins.antismog.smog;

import org.json.JSONException;

import java.io.IOException;

public interface AirlyClient {
    SmogMeasurment getMeasurements() throws IOException, JSONException;
}
