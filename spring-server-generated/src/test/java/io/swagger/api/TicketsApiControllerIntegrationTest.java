package io.swagger.api;

import io.swagger.model.Ticket;

import java.util.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TicketsApiControllerIntegrationTest {

    @Autowired
    private TicketsApi api;

    @Test
    public void buyTicketTest() throws Exception {
        Ticket body = new Ticket();
        ResponseEntity<Void> responseEntity = api.buyTicket(body);
        assertEquals(HttpStatus.NOT_IMPLEMENTED, responseEntity.getStatusCode());
    }

    @Test
    public void getTicketsTest() throws Exception {
        ResponseEntity<List<Ticket>> responseEntity = api.getTickets();
        assertEquals(HttpStatus.NOT_IMPLEMENTED, responseEntity.getStatusCode());
    }

}
