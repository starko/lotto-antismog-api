package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.model.WeatherInfo;
import java.math.BigDecimal;
import org.threeten.bp.OffsetDateTime;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * CityInfo
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2018-11-24T19:47:27.622Z[GMT]")

public class CityInfo   {
  @JsonProperty("name")
  private String name = null;

  @JsonProperty("smogLevel")
  private BigDecimal smogLevel = null;

  @JsonProperty("weather")
  private WeatherInfo weather = null;

  @JsonProperty("currentTime")
  private OffsetDateTime currentTime = null;

  public CityInfo name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(value = "")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public CityInfo smogLevel(BigDecimal smogLevel) {
    this.smogLevel = smogLevel;
    return this;
  }

  /**
   * Get smogLevel
   * @return smogLevel
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getSmogLevel() {
    return smogLevel;
  }

  public void setSmogLevel(BigDecimal smogLevel) {
    this.smogLevel = smogLevel;
  }

  public CityInfo weather(WeatherInfo weather) {
    this.weather = weather;
    return this;
  }

  /**
   * Get weather
   * @return weather
  **/
  @ApiModelProperty(value = "")

  @Valid

  public WeatherInfo getWeather() {
    return weather;
  }

  public void setWeather(WeatherInfo weather) {
    this.weather = weather;
  }

  public CityInfo currentTime(OffsetDateTime currentTime) {
    this.currentTime = currentTime;
    return this;
  }

  /**
   * Get currentTime
   * @return currentTime
  **/
  @ApiModelProperty(value = "")

  @Valid

  public OffsetDateTime getCurrentTime() {
    return currentTime;
  }

  public void setCurrentTime(OffsetDateTime currentTime) {
    this.currentTime = currentTime;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CityInfo cityInfo = (CityInfo) o;
    return Objects.equals(this.name, cityInfo.name) &&
        Objects.equals(this.smogLevel, cityInfo.smogLevel) &&
        Objects.equals(this.weather, cityInfo.weather) &&
        Objects.equals(this.currentTime, cityInfo.currentTime);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, smogLevel, weather, currentTime);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CityInfo {\n");
    
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    smogLevel: ").append(toIndentedString(smogLevel)).append("\n");
    sb.append("    weather: ").append(toIndentedString(weather)).append("\n");
    sb.append("    currentTime: ").append(toIndentedString(currentTime)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

