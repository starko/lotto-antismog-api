package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * UserInfo
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2018-11-24T19:47:27.622Z[GMT]")

public class UserInfo   {
  @JsonProperty("newTicketMultiplier")
  private BigDecimal newTicketMultiplier = null;

  @JsonProperty("drawCount")
  private BigDecimal drawCount = null;

  public UserInfo newTicketMultiplier(BigDecimal newTicketMultiplier) {
    this.newTicketMultiplier = newTicketMultiplier;
    return this;
  }

  /**
   * Get newTicketMultiplier
   * @return newTicketMultiplier
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getNewTicketMultiplier() {
    return newTicketMultiplier;
  }

  public void setNewTicketMultiplier(BigDecimal newTicketMultiplier) {
    this.newTicketMultiplier = newTicketMultiplier;
  }

  public UserInfo drawCount(BigDecimal drawCount) {
    this.drawCount = drawCount;
    return this;
  }

  /**
   * liczba losow
   * @return drawCount
  **/
  @ApiModelProperty(value = "liczba losow")

  @Valid

  public BigDecimal getDrawCount() {
    return drawCount;
  }

  public void setDrawCount(BigDecimal drawCount) {
    this.drawCount = drawCount;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UserInfo userInfo = (UserInfo) o;
    return Objects.equals(this.newTicketMultiplier, userInfo.newTicketMultiplier) &&
        Objects.equals(this.drawCount, userInfo.drawCount);
  }

  @Override
  public int hashCode() {
    return Objects.hash(newTicketMultiplier, drawCount);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UserInfo {\n");
    
    sb.append("    newTicketMultiplier: ").append(toIndentedString(newTicketMultiplier)).append("\n");
    sb.append("    drawCount: ").append(toIndentedString(drawCount)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

