package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WeatherInfo
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2018-11-24T19:47:27.622Z[GMT]")

public class WeatherInfo   {
  @JsonProperty("temp")
  private BigDecimal temp = null;

  @JsonProperty("kind")
  private String kind = null;

  @JsonProperty("icon")
  private String icon = null;

  public WeatherInfo temp(BigDecimal temp) {
    this.temp = temp;
    return this;
  }

  /**
   * temperatura
   * @return temp
  **/
  @ApiModelProperty(value = "temperatura")

  @Valid

  public BigDecimal getTemp() {
    return temp;
  }

  public void setTemp(BigDecimal temp) {
    this.temp = temp;
  }

  public WeatherInfo kind(String kind) {
    this.kind = kind;
    return this;
  }

  /**
   * opis np \"Pochmurno\"
   * @return kind
  **/
  @ApiModelProperty(value = "opis np \"Pochmurno\"")


  public String getKind() {
    return kind;
  }

  public void setKind(String kind) {
    this.kind = kind;
  }

  public WeatherInfo icon(String icon) {
    this.icon = icon;
    return this;
  }

  /**
   * id ikony z open weather map
   * @return icon
  **/
  @ApiModelProperty(value = "id ikony z open weather map")


  public String getIcon() {
    return icon;
  }

  public void setIcon(String icon) {
    this.icon = icon;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WeatherInfo weatherInfo = (WeatherInfo) o;
    return Objects.equals(this.temp, weatherInfo.temp) &&
        Objects.equals(this.kind, weatherInfo.kind) &&
        Objects.equals(this.icon, weatherInfo.icon);
  }

  @Override
  public int hashCode() {
    return Objects.hash(temp, kind, icon);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WeatherInfo {\n");
    
    sb.append("    temp: ").append(toIndentedString(temp)).append("\n");
    sb.append("    kind: ").append(toIndentedString(kind)).append("\n");
    sb.append("    icon: ").append(toIndentedString(icon)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

