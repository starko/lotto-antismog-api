package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import org.threeten.bp.OffsetDateTime;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Ticket
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2018-11-24T19:47:27.622Z[GMT]")

public class Ticket   {
  @JsonProperty("qrCode")
  private String qrCode = null;

  @JsonProperty("validThru")
  private OffsetDateTime validThru = null;

  @JsonProperty("buyTime")
  private OffsetDateTime buyTime = null;

  @JsonProperty("multiplyer")
  private BigDecimal multiplyer = null;

  public Ticket qrCode(String qrCode) {
    this.qrCode = qrCode;
    return this;
  }

  /**
   * Get qrCode
   * @return qrCode
  **/
  @ApiModelProperty(value = "")


  public String getQrCode() {
    return qrCode;
  }

  public void setQrCode(String qrCode) {
    this.qrCode = qrCode;
  }

  public Ticket validThru(OffsetDateTime validThru) {
    this.validThru = validThru;
    return this;
  }

  /**
   * Get validThru
   * @return validThru
  **/
  @ApiModelProperty(value = "")

  @Valid

  public OffsetDateTime getValidThru() {
    return validThru;
  }

  public void setValidThru(OffsetDateTime validThru) {
    this.validThru = validThru;
  }

  public Ticket buyTime(OffsetDateTime buyTime) {
    this.buyTime = buyTime;
    return this;
  }

  /**
   * Get buyTime
   * @return buyTime
  **/
  @ApiModelProperty(value = "")

  @Valid

  public OffsetDateTime getBuyTime() {
    return buyTime;
  }

  public void setBuyTime(OffsetDateTime buyTime) {
    this.buyTime = buyTime;
  }

  public Ticket multiplyer(BigDecimal multiplyer) {
    this.multiplyer = multiplyer;
    return this;
  }

  /**
   * Get multiplyer
   * @return multiplyer
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getMultiplyer() {
    return multiplyer;
  }

  public void setMultiplyer(BigDecimal multiplyer) {
    this.multiplyer = multiplyer;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Ticket ticket = (Ticket) o;
    return Objects.equals(this.qrCode, ticket.qrCode) &&
        Objects.equals(this.validThru, ticket.validThru) &&
        Objects.equals(this.buyTime, ticket.buyTime) &&
        Objects.equals(this.multiplyer, ticket.multiplyer);
  }

  @Override
  public int hashCode() {
    return Objects.hash(qrCode, validThru, buyTime, multiplyer);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Ticket {\n");
    
    sb.append("    qrCode: ").append(toIndentedString(qrCode)).append("\n");
    sb.append("    validThru: ").append(toIndentedString(validThru)).append("\n");
    sb.append("    buyTime: ").append(toIndentedString(buyTime)).append("\n");
    sb.append("    multiplyer: ").append(toIndentedString(multiplyer)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

