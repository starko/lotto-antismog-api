#!/usr/bin/env bash

ENV="http://localhost:8080"
if [[ "$1" == "prod" ]]; then
   ENV="https://lotto-antismog-api.herokuapp.com"
fi
echo $ENV

curl -X POST $ENV/buy-ticket --header "Content-Type:application/json" --data '{"userId":10,"ticketType":"SINGLE_20","reduced":true, "drawCount":3}';